$(function() {
    var APPLICATION_ID = "8B8A7113-84C5-1EC2-FF72-81D12EFC9C00",
        SECRET_KEY = "59276609-A95C-D49C-FFD7-2BC698F7B000",
        VERSION = "v1";
        
        Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
        
        var postCollection = Backendless.Persistence.of(Posts).find();
        console.log(postCollection);
        
        var wrapper = {
            posts: postCollection.data
        };
        
        Handlebars.registerHelper('format', function (time){
            return moment(time).format("dddd, MMMM Do YYYY");
        });
        
        var blogScript =$ ("#blogs-template").html(); 
        var blogTemplate = Handlebars.compile(blogScript);
        var blogHTML = blogTemplate(wrapper);
        
        $('.main-container').html(blogHTML);
                      
});

function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}


